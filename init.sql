CREATE USER 'root'@'%' IDENTIFIED BY 'root';
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%';
FLUSH PRIVILEGES;

CREATE DATABASE school;

USE school;

CREATE TABLE students(
	id INTEGER primary key,
	first_name TEXT,
	last_name TEXT,
	age INTEGER, default 0
);

СREATE TABLE gradebook(
	studentId INTEGER primary key,
	
	1_lesson TEXT,
	1_date TEXT,
	1_mark INTEGER,
	
	2_lesson TEXT,
	2_date TEXT,
	2_mark INTEGER,
	
	3_lesson TEXT,
	3_date TEXT,
	3_mark INTEGER,
	
	4_lesson TEXT,
	4_date TEXT,
	4_mark INTEGER,
	
	5_lesson TEXT,
	5_date TEXT,
	5_mark INTEGER,
)
INSERT INTO students VALUES (0, 'Eric', 'Rivers', 18);
INSERT INTO students VALUES (1, 'Molly', 'Perala', 20);
INSERT INTO students VALUES (2, 'John', 'Dyer', 25);
INSERT INTO students VALUES (3, 'Kate', 'Rios', 17);
INSERT INTO students VALUES (4, 'Jill', 'Little', 35);

INSERT INTO gradebook VALUES (0, 'Math', '10/12/2022', Null,
'Biology', '10/12/2022', 3,
'Literature', '10/12/2022', Null,
'Russian Language', '10/12/2022', 5,
'Physics', '10/12/2022', 4);

INSERT INTO gradebook VALUES (1, 'Math', '10/12/2022', 3,
'Biology', '10/12/2022', 3,
'Literature', '10/12/2022', Null,
'Russian Language', '10/12/2022', Null,
'Physics', '10/12/2022', 4);

INSERT INTO gradebook VALUES (2, 'Math', '10/12/2022', 4,
'Biology', '10/12/2022', Null,
'Literature', '10/12/2022', 4,
'Russian Language', '10/12/2022', 5,
'Physics', '10/12/2022', 4);

INSERT INTO gradebook VALUES (3, 'Math', '10/12/2022', 3,
'Biology', '10/12/2022', Null,
'Literature', '10/12/2022', 4,
'Russian Language', '10/12/2022', 5,
'Physics', '10/12/2022', 2);

INSERT INTO gradebook VALUES (4, 'Math', '10/12/2022', 4,
'Biology', '10/12/2022', 4,
'Literature', '10/12/2022', 4,
'Russian Language', '10/12/2022', 5,
'Physics', '10/12/2022', 4);
COMMIT;
